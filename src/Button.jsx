import clsx from 'clsx';
import PropTypes from 'prop-types';

const Button = ({
  variant = 'primary',
  className = '',
  large = false,
  onClick = () => {},
}) => (
  <button
    className={clsx('react-button', variant, large && 'large', className)}
    onClick={onClick}
    type="button"
  >
    Klikni
  </button>
);

Button.propTypes = {
  variant: PropTypes.string,
  className: PropTypes.string,
  large: PropTypes.bool,
  onClick: PropTypes.func,
};

export default Button;
