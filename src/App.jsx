import Button from './Button';

const App = () => (
  <>
    <h1>Hello, I&apos;m first React jsx functional app!</h1>
    <div className="row">{`${PRODUCTION ? 'Jde' : 'Nejde'} o produkční verzi.`}</div>
    <Button variant="secondary" large className="pill" onClick={console.log} />
  </>
);

export default App;
