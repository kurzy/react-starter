module.exports = {
  extends: ['airbnb'],
  ignorePatterns: [
    '.eslintrc.prod.js',
    '.eslintrc.js',
    'webpack.config.common.js',
    'webpack.config.dev.js',
    'webpack.config.prod.js',
    'postcss.config.js',
    'dist/**/*',
    'jsconfig.json',
  ],
  env: {
    es6: true,
    browser: true,
    node: true,
  },
  globals: {
    PRODUCTION: 'writable',
  },
  settings: {
    'import/resolver': {
      alias: {
        map: [
          ['Utils', './src/utils'],
        ],
      },
    },
  },
  rules: {
    'no-debugger': 'warn',
    'no-console': [
      'warn',
      {
        allow: ['warn', 'error'],
      },
    ],
    curly: ['error', 'all'],
    'brace-style': ['error', '1tbs'],
    'no-continue': 'off',
    'no-await-in-loop': 'off',
    'no-restricted-syntax': 'off',
    'no-restricted-imports': [
      'error',
      {
        paths: [{
          name: 'lodash',
          message: 'Please use lodash/module instead.',
        }],
      },
    ],
    'import/order': [
      'error',
      {
        groups: ['builtin', 'external', 'internal', ['parent', 'sibling']],
        pathGroups: [],
        'newlines-between': 'never',
        alphabetize: {
          order: 'asc',
          caseInsensitive: true,
        },
      },
    ],
    'react/react-in-jsx-scope': 'off',
    'react/function-component-definition': [
      'error',
      {
        namedComponents: 'arrow-function',
      },
    ],
    'react/require-default-props': [
      'error',
      {
        forbidDefaultForRequired: true,
        ignoreFunctionalComponents: true,
      },
    ],
  },
};
